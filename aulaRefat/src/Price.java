
abstract class Price {

    abstract double getCharge(int daysRented);

    abstract int getPriceCode();

    int getFrequentRenterPoints(int daysRented, Movie movie) {
        if ((movie.getPriceCode() == Movie.NEW_RELEASE) && daysRented > 1) {
            return 2;
        } else {
            return 1;
        }
    }

    int getFrequentRenterPoints(int daysRented) {
        return 1;
    }
}
